import { Pagination } from '../../domain/models/pagination.model'
import { Recipe } from '../../domain/models/recipe.model'
import UseCase from '../../domain/models/usecase'
import recipes from '../../../recipes.json'
import GetRecipesRepository from '../../infrastructure/database/repositories/recipe/getRecipesRepository.interface'
import GetRecipesUsecase from './getRecipes.usecase'

interface SutType {
  sut: UseCase<Pagination, Recipe[]>
  repository: GetRecipesRepository
}

const makeRepoStub = (): GetRecipesRepository => {
  class RepoStub implements GetRecipesRepository {
    public async getAll (pagination: Pagination): Promise<Recipe[]> {
      return recipes as unknown as Recipe[]
    }
  }

  return new RepoStub()
}

const makeSut = (): SutType => {
  const repository = makeRepoStub()
  const sut = new GetRecipesUsecase(repository)

  return {
    sut,
    repository
  }
}

describe('GetRecipesUsecase Test', () => {
  test('should return all recipes', async () => {
    const { sut, repository } = makeSut()

    const repositorySpy = jest.spyOn(repository, 'getAll')

    const getRecipes = await sut.execute({ page: '0', perPage: '1' })

    expect(repositorySpy).toBeCalledWith({ page: '0', perPage: '1' })
    expect(getRecipes).toStrictEqual(recipes)
  })
})
