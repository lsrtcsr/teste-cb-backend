import { Pagination } from '../../domain/models/pagination.model'
import { Recipe } from '../../domain/models/recipe.model'
import UseCase from '../../domain/models/usecase'
import GetRecipesRepository from '../../infrastructure/database/repositories/recipe/getRecipesRepository.interface'

export default class GetRecipesUsecase implements UseCase<Pagination, Recipe[]> {
  constructor (private readonly getRecipesRepo: GetRecipesRepository) {}

  public async execute (data: Pagination): Promise<Recipe[]> {
    return await this.getRecipesRepo.getAll(data)
  }
}
