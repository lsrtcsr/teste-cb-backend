import { Recipe } from "../../domain/models/recipe.model";
import UseCase from "../../domain/models/usecase";
import GetRecipeByIdRepository from "../../infrastructure/database/repositories/recipe/getRecipeByIdRepository.interface";

export default class GetRecipeByIdUsecase implements UseCase<string, Recipe> {
	constructor(private readonly getByIdRepository: GetRecipeByIdRepository) {}

    public async execute(id: string): Promise<Recipe> {
        return await this.getByIdRepository.get(id)
    }
}