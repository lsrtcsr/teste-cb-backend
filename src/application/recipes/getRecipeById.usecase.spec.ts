import { Recipe, RecipeModel } from "../../domain/models/recipe.model";
import GetRecipeByIdRepository from "../../infrastructure/database/repositories/recipe/getRecipeByIdRepository.interface";
import GetRecipeByIdUsecase from "./getRecipeById.usecase";
import recipes from '../../../recipes.json'

interface SutType {
	sut: GetRecipeByIdUsecase,
	repository: GetRecipeByIdRepository
}

const makeRepository = (): GetRecipeByIdRepository => {
	class RepoStub implements GetRecipeByIdRepository {
        public async get(id: string): Promise<Recipe> {
        	const recipe = recipes[0] as unknown as RecipeModel
            return {
            	id: 'testid',
            	...recipe
            }
        }
	}

	return new RepoStub()
}

const makeSut = (): SutType => {
	const repository = makeRepository()
	const sut = new GetRecipeByIdUsecase(repository)

	return {
		sut,
		repository
	}
}

describe('GetRecipeByIdUsecase Test', () => {
	test('should return the recipe by id', async () => {
		const {sut, repository} = makeSut()

		const repositorySpy = jest.spyOn(repository, 'get')

		const foundRecipe = await sut.execute('testid')
		expect(repositorySpy).toBeCalledWith('testid')
		expect(foundRecipe.id).toBeDefined()

		const recipe = recipes[0] as unknown as RecipeModel
		expect(foundRecipe).toStrictEqual({
			id: 'testid',
			...recipe
		})
	})
})