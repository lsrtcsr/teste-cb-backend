import UseCase from "../../domain/models/usecase";
import { User, UserModel } from "../../domain/models/user.model";
import Encrypter from "../../domain/protocols/encrypter.interface";
import SignupRepository from "../../infrastructure/database/repositories/signup/signup.interface";

export default class SignupUseCase implements UseCase<UserModel, User> {
    constructor(
        private readonly signupRepository: SignupRepository<UserModel, User>,
        private readonly encrypter: Encrypter
    ) {}

    public async execute(data: UserModel): Promise<User> {
        const encryptedPassword = await this.encrypter.encrypt(data.password)
        const newUser = await this.signupRepository.register({
            ...data,
            password: encryptedPassword
        })
        return newUser
    }	
}