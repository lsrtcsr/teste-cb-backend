import { exitCode } from "process"
import { User, UserModel } from "../../domain/models/user.model"
import Encrypter from "../../domain/protocols/encrypter.interface"
import SignupRepository from "../../infrastructure/database/repositories/signup/signup.interface"
import SignupUseCase from "./signup.usecase"

interface SutType {
	sut: SignupUseCase,
	repository: SignupRepository<UserModel, User>,
	encrypter: Encrypter
}

const makeRepoStub = (): SignupRepository<UserModel, User> => {
	class Repo implements SignupRepository<UserModel, User> {
		public async register(entity: UserModel): Promise<User> {
			return {
				id: 'testid',
				email: 'test@email.com',
				username: 'testusername',
				password: 'encrypted_password'
			}
		}
	}
	return new Repo()
}

const makeEcrypter = (): Encrypter => {
	class StubEncrypter implements Encrypter {
		public async encrypt(value: string): Promise<string> {
			return 'encrypted_password'
		}
	}

	return new StubEncrypter()
}

const makeSut = (): SutType => {
	const repository = makeRepoStub()
	const encrypter = makeEcrypter()
	const sut = new SignupUseCase(repository, encrypter)

	return {
		sut,
		repository,
		encrypter
	}
}

describe('SignupUsecase Test', () => {
	test('should create the user', async () => {
		const { sut, repository, encrypter } = makeSut()

		const repoSpy = jest.spyOn(repository, 'register')
		const encrypterSpy = jest.spyOn(encrypter, 'encrypt')

		const userModel: UserModel = {
			email: 'test@email.com',
			username: 'testusername',
			password: 'testpassword'
		}

		const createdUser = await sut.execute(userModel)

		expect(encrypterSpy).toBeCalledWith(userModel.password)
		expect(repoSpy).toBeCalledWith({ 
			...userModel, 
			password: "encrypted_password" 
		})

		expect(createdUser).toStrictEqual({ ...userModel, password: "encrypted_password", id: 'testid' })
	})
})