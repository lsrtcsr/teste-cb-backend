import { InvalidParamError } from "../../controllers/errors";
import { Signin } from "../../domain/models/signin.model";
import { Token } from "../../domain/models/token.model";
import UseCase from "../../domain/models/usecase";
import Decrypter from "../../domain/protocols/decrypt.interface";
import Tokenizer from "../../domain/protocols/tokenizer.interface";
import SigninRepository from "../../infrastructure/database/repositories/signin/signin.interface";

export default class SigninUseCase implements UseCase<Signin, Token> {
    constructor(
        private readonly signinRepository: SigninRepository,
        private readonly decrypter: Decrypter,
        private readonly tokenizer: Tokenizer
    ) { }

    public async execute(data: Signin): Promise<Token> {
        const foundUser = await this.signinRepository.findByUsername(data.username);
        if (!foundUser) {
            throw new InvalidParamError('username');
        }

        const passwordMatch = await this.decrypter.compare(
            foundUser.password,
            data.password)
        if (!passwordMatch) {
            throw new InvalidParamError('password')
        }

        const token = await this.tokenizer.tokenize({
            id: foundUser.id,
            username: foundUser.username
        })
        
        return {
            token,
            refresh: token
        }
    }
}