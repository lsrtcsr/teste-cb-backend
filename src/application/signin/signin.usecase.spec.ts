import { Signin } from "../../domain/models/signin.model";
import { User } from "../../domain/models/user.model";
import Decrypter from "../../domain/protocols/decrypt.interface";
import Tokenizer from "../../domain/protocols/tokenizer.interface";
import SigninRepository from "../../infrastructure/database/repositories/signin/signin.interface";
import SigninUseCase from "./signin.usecase";

interface SutTypes {
	repository: SigninRepository,
	decrypter: Decrypter,
	tokenizer: Tokenizer,
	sut: SigninUseCase
}

const makeSut = (): SutTypes => {
	const repository = makeRepoStub()
	const decrypter = makeDecrypterStub()
	const tokenizer = makeTokenizerStub()

	const sut = new SigninUseCase(
		repository, decrypter, tokenizer)

	return {
		repository,
		decrypter,
		tokenizer,
		sut
	}
}

const makeRepoStub = (): SigninRepository => {
	class RepoStub implements SigninRepository {
		public async findByUsername(u: string): Promise<User> {
			return {
				id: 'testid',
				username: 'testusername',
				email: 'test@test.com',
				password: 'encrypet_password'
			}
		}
	}

	return new RepoStub()
}

const makeDecrypterStub = (): Decrypter => {
	class DecrypterStub implements Decrypter {
		public async compare(encPassword: string, passwprd: string): Promise<boolean> {
			return true
		}
	}

	return new DecrypterStub()
}

const makeTokenizerStub = (): Tokenizer => {
	class TokenizerStub implements Tokenizer {
		public async tokenize(data: any): Promise<string> {
			return 'auth_token'
		}
	}

	return new TokenizerStub()
}

describe('SigninUseCase Test', () => {
	test('should sigin the user', async () => {
		const { sut, decrypter, repository, tokenizer } = makeSut()

		const decrypterSpy = jest.spyOn(decrypter, 'compare')
		const repositorySpy = jest.spyOn(repository, 'findByUsername')
		const tokenizerSpy = jest.spyOn(tokenizer, 'tokenize')

		const signin: Signin = {
			username: 'testusername',
			password: 'password'
		}

		const response = await sut.execute(signin)

		expect(repositorySpy).toBeCalledWith(signin.username)
		expect(decrypterSpy).toBeCalledWith("encrypet_password", signin.password)
		expect(tokenizerSpy).toBeCalledWith({ id: 'testid', username: signin.username })

		expect(response).toStrictEqual({
			token: "auth_token",
			refresh: 'auth_token'
		})
	})
})



