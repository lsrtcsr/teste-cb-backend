import GetRecipeByIdUsecase from "../../application/recipes/getRecipeById.usecase";
import GetRecipeByIdController from "../../controllers/handlers/getRecipesById.controller";
import HttpControllerFactory from "../../controllers/model/httpController.factory";
import GetRecipeByIdRepositoryImpl from "../database/repositories/recipe/getRecipeByIdRepositoryImpl.repository";

export default class GetRecipeByIdControllerFactory implements HttpControllerFactory<GetRecipeByIdController> {
    public create(): GetRecipeByIdController {
        return new GetRecipeByIdController(new GetRecipeByIdUsecase(new GetRecipeByIdRepositoryImpl))
    }
}