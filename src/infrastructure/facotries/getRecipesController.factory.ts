import GetRecipesUsecase from '../../application/recipes/getRecipes.usecase'
import GetRecipesController from '../../controllers/handlers/getRecipes.controller'
import HttpControllerFactory from '../../controllers/model/httpController.factory'
import GetRecipesRepositoryImpl from '../database/repositories/recipe/getRecipesRepositoryImpl.repository'

export default class GetRecipesControllerFactory implements HttpControllerFactory<GetRecipesController> {
  public create (): GetRecipesController {
    return new GetRecipesController(new GetRecipesUsecase(new GetRecipesRepositoryImpl()))
  }
}
