import SignupUseCase from '../../application/signup/signup.usecase'
import HttpControllerFactory from '../../controllers/model/httpController.factory'
import SignupController from '../../controllers/handlers/signup.controller'
import SignupRepositoryImpl from '../database/repositories/signup/signup.repository'
import BcryptAdapter from '../adapter/bcrypt.adapter'

export default class SignupControllerFactory implements HttpControllerFactory<SignupController> {
  public create (): SignupController {
    return new SignupController(
      new SignupUseCase(
        new SignupRepositoryImpl(),
        new BcryptAdapter()))
  }
}
