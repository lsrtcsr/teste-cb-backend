import SigninUseCase from '../../application/signin/signin.usecase'
import SigninController from '../../controllers/handlers/signin.controller'
import HttpControllerFactory from '../../controllers/model/httpController.factory'
import BcryptAdapter from '../adapter/bcrypt.adapter'
import JwtAdapter from '../adapter/jwt.adapter'
import SigninReposiotryImpl from '../database/repositories/signin/signin.repository'

export default class SigninControllerFactory implements HttpControllerFactory<SigninController> {
  public create (): SigninController {
    return new SigninController(
      new SigninUseCase(
        new SigninReposiotryImpl(),
        new BcryptAdapter(),
        new JwtAdapter()))
  }
}
