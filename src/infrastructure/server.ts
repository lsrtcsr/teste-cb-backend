import dotenv from 'dotenv'
import app from './app'
import { Mongo } from './database/mongodb'

dotenv.config()

const mongoUrl = process.env.MONGODB_URL as string
const port = process.env.PORT as string

Mongo.connect(mongoUrl).then(async () => {
  app.listen(port, () => {
    console.log(`Server running on port ${port}`)
  })
}).catch(console.error)
