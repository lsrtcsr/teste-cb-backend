import express from 'express'

import buildRoutes from './config/routes'
import buildMiddlewares from './config/middlewares'

const app = express()

buildMiddlewares(app)
buildRoutes(app)

export default app
