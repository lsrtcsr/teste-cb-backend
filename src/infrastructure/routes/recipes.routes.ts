import { Router } from 'express'
import { RouterAdapter } from '../adapter/expressAdapterRoutes'
import GetRecipeByIdControllerFactory from '../facotries/getRecipeByIdController.factory'
import GetRecipesControllerFactory from '../facotries/getRecipesController.factory'

export default (r: Router): void => {
  const getRecipesControllerFactory = new GetRecipesControllerFactory()
  const getRecipesController = new RouterAdapter().adapt(getRecipesControllerFactory.create())

  const getRecipeByIdFactory = new GetRecipeByIdControllerFactory()
  const getRecipeByIdController = new RouterAdapter().adapt(getRecipeByIdFactory.create())

  r.get('/recipes', getRecipesController)
  r.get('/recipes/:recipeId', getRecipeByIdController)
}
