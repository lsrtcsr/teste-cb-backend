import { Router } from "express";
import { RouterAdapter } from "../adapter/expressAdapterRoutes";
import SignupControllerFactory from "../facotries/signup.factory";

export default (r: Router): void => {
	const httpControlelrFactory = new SignupControllerFactory()
	const controller = new RouterAdapter().adapt(httpControlelrFactory.create())

	r.post('/signup', controller)
}