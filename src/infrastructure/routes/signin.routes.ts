import { Router } from "express";
import { RouterAdapter } from "../adapter/expressAdapterRoutes";
import SigninControllerFactory from "../facotries/signin.factory";

export default (r: Router): void => {
	const httpControllerFactory = new SigninControllerFactory()
	const controller = new RouterAdapter().adapt(httpControllerFactory.create())

	r.post('/signin', controller)	 
}