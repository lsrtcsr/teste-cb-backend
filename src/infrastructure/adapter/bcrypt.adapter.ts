import bcrypt from 'bcrypt'
import Decrypter from '../../domain/protocols/decrypt.interface'
import Encrypter from '../../domain/protocols/encrypter.interface'

export default class BcryptAdapter implements Encrypter, Decrypter {
  public async compare (encPassword: string, passwprd: string): Promise<boolean> {
    return await bcrypt.compare(passwprd, encPassword)
  }

  public async encrypt (value: string): Promise<string> {
    const salt = process.env.SALT as string
    const hash = await bcrypt.hash(value, parseInt(salt))
    return hash
  }
}
