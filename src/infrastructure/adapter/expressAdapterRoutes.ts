import { Controller } from '../../controllers/model/controller.interface'
import { HttpRequest, HttpResponse } from '../../controllers/model/http.interface'
import { RequestHandler, Request, Response } from 'express'

export class RouterAdapter<T extends Controller<HttpRequest, HttpResponse>> {
  public adapt (controller: T): RequestHandler {
    return async (request: Request, response: Response) => {
      const result = await controller.handle({
        query: request.query,
        body: request.body,
        params: request.params
      })

      response.status(result.statusCode).json(result.body)
    }
  }
}
