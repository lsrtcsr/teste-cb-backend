import jwt from 'jsonwebtoken'
import Tokenizer from '../../domain/protocols/tokenizer.interface'

export default class JwtAdapter implements Tokenizer {
  public async tokenize (data: any): Promise<string> {
    const secret = process.env.SECRET as string
    const expiration = process.env.EXPIRATION as string

    return jwt.sign(data, secret, {
      expiresIn: parseInt(expiration)
    })
  }
}
