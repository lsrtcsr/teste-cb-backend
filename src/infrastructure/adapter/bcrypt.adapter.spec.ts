import bcrypt from 'bcrypt'
import BcryptAdapter from './bcrypt.adapter'

jest.mock('bcrypt', () => ({
  hash: async (): Promise<string> => {
    return 'any_hash'
  }
}))

const makeSut = (): BcryptAdapter => {
  const sut = new BcryptAdapter()
  return sut
}
describe('Bcrypt Adapter Test', () => {
  test('Should call bcrypt adapter with correct values', async () => {
    const sut = makeSut()

    const hashSpy = jest.spyOn(bcrypt, 'hash')
    await sut.encrypt('any_value')

    expect(hashSpy).toHaveBeenCalledWith('any_value', 12)
  })

  test('Should call bcrypt adapter with correct values', async () => {
    const sut = makeSut()
    const hash = await sut.encrypt('any_value')
    expect(hash).toEqual('any_hash')
  })

  test('Should call bcrypt adapter with correct values', async () => {
    const sut = makeSut()

    const error = new Error()
    jest.spyOn(bcrypt, 'hash').mockRejectedValue(error as never)
    const promise = sut.encrypt('any_value')

    await expect(promise).rejects.toThrow()
  })
})