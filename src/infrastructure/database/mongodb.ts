import { MongoClient, Collection } from 'mongodb'

export class Mongo {
  private static client: MongoClient | null

  public static async connect (uri: string): Promise<void> {
    Mongo.client = await MongoClient.connect(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
  }

  public static async disconnect (): Promise<void> {
    if (Mongo.client) {
      await Mongo.client.close()
      Mongo.client = null
    }
  }

  public static async getCollection (name: string): Promise<Collection> {
    if (!Mongo.client?.isConnected()) {
      const uri = process.env.MONGO_URL as string
      await Mongo.connect(uri)
    }

    return Mongo.client!.db().collection(name)
  }

  public static map<T>(collection: any): T {
    const { _id, ...data } = collection
    return Object.assign({}, data, { id: _id })
  }
}
