import { User, UserModel } from '../../../../domain/models/user.model'
import { Mongo } from '../../mongodb'
import SignupRepository from './signup.interface'

export default class SignupRepositoryImpl implements SignupRepository<UserModel, User> {
  public async register (entity: UserModel): Promise<User> {
    const collection = await Mongo.getCollection('users')
    await collection.createIndex({ username: 1 }, { unique: true })

    const result = await collection.insertOne(entity)
    return await Mongo.map(result.ops[0])
  }
}
