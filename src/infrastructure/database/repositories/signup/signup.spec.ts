import { Mongo } from '../../mongodb'
import SignupRepositoryImpl from './signup.repository'

const makeSut = (): SignupRepositoryImpl => {
  return new SignupRepositoryImpl()
}

describe('SignupRepository Test', () => {
  beforeAll(async () => {
    await Mongo.connect(process.env.MONGODB_URL as string)
  })

  afterAll(async () => {
    await Mongo.disconnect()
  })

  beforeEach(async () => {
    const collection = await Mongo.getCollection('users')
    await collection.deleteMany({})
  })

  describe('#register', () => {
    test('should return an user	on success', async () => {
      const sut = makeSut()

      const user = await sut.register({
        username: 'testusername',
        password: 'testpassword',
        email: 'test@email.com'
      })

      expect(user).toBeDefined()
      expect(user.id).toBeDefined()
      expect(user.username).toBe('testusername')
      expect(user.password).toBe('testpassword')
      expect(user.email).toBe('test@email.com')
    })
  })
})
