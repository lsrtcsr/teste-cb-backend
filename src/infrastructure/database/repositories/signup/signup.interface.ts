import Repository from '../../repository.interface'

export default interface SignupRepository<T, O> extends Repository<T, O> {
  register(entity: T): Promise<O>
}
