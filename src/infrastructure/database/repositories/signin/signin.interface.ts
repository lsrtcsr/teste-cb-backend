import { Signin } from '../../../../domain/models/signin.model'
import { User } from '../../../../domain/models/user.model'
import Repository from '../../repository.interface'

export default interface SigninRepository extends Repository<Signin, User> {
  findByUsername(u: string): Promise<User>
}
