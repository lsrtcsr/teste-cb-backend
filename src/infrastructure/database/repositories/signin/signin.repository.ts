import { User } from '../../../../domain/models/user.model'
import { Mongo } from '../../mongodb'
import SigninRepository from './signin.interface'

export default class SigninReposiotryImpl implements SigninRepository {
  public async findByUsername (u: string): Promise<User> {
    const collection = await Mongo.getCollection('users')
    const result = await collection.findOne({ username: u })
    return await Mongo.map(result)
  }
}
