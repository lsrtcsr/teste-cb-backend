import { Collection } from 'mongodb'
import { Mongo } from '../../mongodb'
import SignupRepositoryImpl from '../signup/signup.repository'
import SigninRepository from './signin.interface'
import SigninReposiotryImpl from './signin.repository'

const makeSut = (): SigninRepository => {
  return new SigninReposiotryImpl()
}

describe('SigninReposiotry Test', () => {
  let collection: Collection
  beforeAll(async () => {
    await Mongo.connect(process.env.MONGODB_URL as string)
  })

  afterAll(async () => {
    await Mongo.disconnect()
  })

  beforeEach(async () => {
    collection = await Mongo.getCollection('users')
    await new SignupRepositoryImpl().register({
      username: 'testusername1',
      password: 'testpassword',
      email: 'test@email.com'
    })
  })

  afterEach(async () => {
    await collection.deleteMany({})
  })

  test('should find a user by username', async () => {
    const sut = makeSut()

    const user = await sut.findByUsername('testusername1')

    expect(user.id).toBeDefined()
    expect(user.username).toBe('testusername1')
  })
})
