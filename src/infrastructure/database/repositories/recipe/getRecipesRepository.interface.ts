import { Pagination } from '../../../../domain/models/pagination.model'
import { Recipe } from '../../../../domain/models/recipe.model'
import Repository from '../../repository.interface'

export default interface GetRecipesRepository extends Repository<Pagination, Recipe> {
  getAll(pagination: Pagination): Promise<Recipe[]>
}
