import { Collection } from 'mongodb'
import { Mongo } from '../../mongodb'

import recipes from '../../../../../recipes.json'
import GetRecipesRepositoryImpl from './getRecipesRepositoryImpl.repository'
import GetRecipesRepository from './getRecipesRepository.interface'

const makeSut = (): GetRecipesRepository => {
  return new GetRecipesRepositoryImpl()
}

describe('GetRecipesRepositoryImpl Test', () => {
  let collection: Collection

  beforeAll(async () => {
    await Mongo.connect(process.env.MONGODB_URL as string)
  })

  afterAll(async () => {
    await Mongo.disconnect()
  })

  beforeEach(async () => {
    collection = await Mongo.getCollection('recipes')
    await collection.insertMany(recipes)
  })

  afterEach(async () => {
    await collection.deleteMany({})
  })

  test('should return recipes', async () => {
    const sut = makeSut()

    const recipes = await sut.getAll({
      page: '1',
      perPage: '1'
    })

    expect(recipes.length).toBe(1)
  })

  test('should return all recipes', async () => {
    const sut = makeSut()

    const recipes = await sut.getAll({})

    expect(recipes.length).toBe(1)
  })
})
