import { Recipe } from "../../../../domain/models/recipe.model";
import { Mongo } from "../../mongodb";
import GetRecipeByIdRepository from "./getRecipeByIdRepository.interface";
import mongodb from 'mongodb'

export default class GetRecipeByIdRepositoryImpl implements GetRecipeByIdRepository {
    public async get(id: string): Promise<Recipe> {
        const collection = await Mongo.getCollection('recipes')

        const mongoid = new mongodb.ObjectID(id)
        const recipe = await collection.findOne({"_id": mongoid})

        return Mongo.map<Recipe>(recipe)
    }
}
