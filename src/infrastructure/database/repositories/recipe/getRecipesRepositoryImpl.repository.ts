import { Cursor } from 'mongodb'
import { Pagination } from '../../../../domain/models/pagination.model'
import { Recipe } from '../../../../domain/models/recipe.model'
import { Mongo } from '../../mongodb'

export default class GetRecipesRepositoryImpl implements GetRecipesRepositoryImpl {
  public async getAll (pagination: Pagination): Promise<Recipe[]> {
    const collection = await Mongo.getCollection('recipes')

    const { page, perPage } = pagination
    if (page && perPage) {
      const parsedPage = parseInt(page)
      const parsedPerPage = parseInt(perPage)

      const recipes = collection.find().skip((parsedPage - 1) * parsedPerPage).limit(parsedPerPage)
      return await this.mapRecipes(recipes)
    }

    return await this.mapRecipes(collection.find())
  }

  private async mapRecipes (recipesCurson: Cursor<any>): Promise<Recipe[]> {
    const mappedRecipes = await recipesCurson.map(r => Mongo.map<Recipe>(r)).toArray()
    return mappedRecipes
  }
}
