import { Recipe } from '../../../../domain/models/recipe.model'
import Repository from '../../repository.interface'

export default interface GetRecipeByIdRepository extends Repository<string, Recipe> {
  get(id: string): Promise<Recipe>
}
