import { Collection } from 'mongodb'
import { Mongo } from '../../mongodb'

import recipes from '../../../../../recipes.json'
import GetRecipeByIdRepository from './getRecipeByIdRepository.interface'
import GetRecipeByIdRepositoryImpl from './getRecipeByIdRepositoryImpl.repository'

const makeSut = (): GetRecipeByIdRepository => {
  return new GetRecipeByIdRepositoryImpl()
}

describe.only('GetRecipeByIdRepositoryImpl Test', () => {
  let collection: Collection
  let insertedId: any

  beforeAll(async () => {
    await Mongo.connect(process.env.MONGODB_URL as string)
  })

  afterAll(async () => {
    await Mongo.disconnect()
  })

  beforeEach(async () => {
    collection = await Mongo.getCollection('recipes')
    const inserResult = await collection.insertOne(recipes[0])
    insertedId = inserResult.insertedId
  })

  afterEach(async () => {
    await collection.deleteMany({})
  })

  test('should return recipes', async () => {
    const sut = makeSut()
    const foundRecipe = await sut.get(insertedId)
    expect(foundRecipe).toBeDefined()
  })
})
