import express, { Express, NextFunction, Response, Request } from 'express'

const cors = (req: Request, res: Response, next: NextFunction): void => {
  res.set('access-control-allow-origin', '*')
  res.set('access-control-allow-header', '*')
  res.set('access-control-allow-methods', '*')
  next()
}

const contentType = (req: Request, res: Response, next: NextFunction): void => {
  res.type('application/json')
  next()
}

const logger = (req: Request, res: Response, next: NextFunction): void => {
  console.log("test")
  next()
}

export default (app: Express): void => {
  app.use(contentType)
  app.use(express.json())
  app.use(cors)
  app.use(logger)
}