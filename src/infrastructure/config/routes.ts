import { Express, Router } from 'express'
import fg from 'fast-glob'

export default (app: Express): void => {
  const router = Router()
  app.use('/api', router)

  fg.sync('**/src/infrastructure/routes/**routes.ts').map(async file => {
      console.log(`Building route for ${file}`)
      return (await import(`../../../${file}`)).default(router)
    }
  )
}