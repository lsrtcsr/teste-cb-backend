export class NotFoundError extends Error {
  constructor (param: string) {
    super(`Record with id ${param} not found`)
    this.name = 'NotFoundError'
  }
}