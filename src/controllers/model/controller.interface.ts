import { HttpRequest, HttpResponse } from "./http.interface";

export interface Controller<T extends HttpRequest, O extends HttpResponse> {
	handle(request: T): Promise<O>
}