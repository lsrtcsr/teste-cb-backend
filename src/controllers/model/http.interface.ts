export type HttpRequest = {
  query?: any
  body?: any,
  params?: any
}

export type HttpResponse = {
statusCode: number
  body: any
}