import { Controller } from "./controller.interface";
import { HttpRequest, HttpResponse } from "./http.interface";

export default interface HttpControllerFactory<T extends Controller<HttpRequest, HttpResponse>> {
	create(): T	
}