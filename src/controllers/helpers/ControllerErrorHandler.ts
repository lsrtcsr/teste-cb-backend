
import { InvalidParamError, MissingParamError, NotFoundError } from '../errors'
import { HttpRequest, HttpResponse } from '../model/http.interface'
import { BadRequest, InternalServerError, NotFound } from './http.helpers'

export default function ControllerErrorHandler (target: Object, propertyName: string, propertyDesciptor: PropertyDescriptor): PropertyDescriptor {
  const method = propertyDesciptor.value

  propertyDesciptor.value = async function (...args: HttpRequest[]): Promise<HttpResponse> {
    try {
      const result = await method.apply(this, args)
      return result
    } catch (error) {
      console.error(error)
      if (error instanceof MissingParamError) {
        return BadRequest(error)
      }
      if (error instanceof InvalidParamError) {
        return BadRequest(error)
      }
      if (error instanceof NotFoundError) {
        return NotFound(error)
      }

      return InternalServerError()
    }
  }

  return propertyDesciptor
}