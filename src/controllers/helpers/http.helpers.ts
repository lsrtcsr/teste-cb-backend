import { ServerError } from "../errors"
import { HttpResponse } from "../model/http.interface"

export const BadRequest = (error: Error): HttpResponse => ({
  statusCode: 400,
  body: error
})

export const InternalServerError = (): HttpResponse => ({
  statusCode: 500,
  body: new ServerError()
})

export const OK = (data: any): HttpResponse => ({
  statusCode: 200,
  body: data
})

export const NotFound = (data: any): HttpResponse => ({
  statusCode: 404,
  body: data
})