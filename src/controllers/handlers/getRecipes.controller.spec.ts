import { Pagination } from '../../domain/models/pagination.model'
import { Recipe } from '../../domain/models/recipe.model'
import UseCase from '../../domain/models/usecase'
import recipes from '../../../recipes.json'
import { HttpRequest } from '../model/http.interface'
import GetRecipesController from './getRecipes.controller'

interface SutType {
  sut: GetRecipesController
  usecase: UseCase<Pagination, Recipe[]>
}

const makeUsecase = (): UseCase<Pagination, Recipe[]> => {
  class UsecaseStub implements UseCase<Pagination, Recipe[]> {
    public async execute (data: Pagination): Promise<Recipe[]> {
      return recipes as unknown as Recipe[]
    }
  }

  return new UsecaseStub()
}

const makeSut = (): SutType => {
  const usecase = makeUsecase()
  const sut = new GetRecipesController(usecase)

  return {
    sut,
    usecase
  }
}

describe('GetRecipesController Test', () => {
  test('should return recipes', async () => {
    const { sut, usecase } = makeSut()

    const usecaseSpy = jest.spyOn(usecase, 'execute')

    const request: HttpRequest = {
      query: {
        page: '1',
        perPage: '1'
      }
    }

    const getRecipes = await sut.handle(request)

    expect(usecaseSpy).toBeCalledWith(request.query)
    expect(getRecipes.body).toBeDefined()
    expect(getRecipes.statusCode).toBe(200)
  })
})
