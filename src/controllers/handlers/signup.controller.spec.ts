import UseCase from "../../domain/models/usecase";
import { User, UserModel } from "../../domain/models/user.model";
import { InvalidParamError, ServerError } from "../errors";
import SignupController from "./signup.controller";

interface SutType {
	sut: SignupController
	signupUsecaseStub: UseCase<UserModel, User>
}

const makeSignupUsecaseStub = (): UseCase<UserModel, User> => {
	class SignupUseCaseStub implements UseCase<UserModel, User> {
		public async execute(data: UserModel): Promise<User> {
			return {
				id: 'testid',
				email: 'any_email@gmail.com',
				username: "username",
				password: 'any_password',
			}
		}
	}

	return new SignupUseCaseStub()
}

const makeSut = (): SutType => {
	const signupUsecaseStub = makeSignupUsecaseStub()
	const sut = new SignupController(signupUsecaseStub)

	return {
		sut,
		signupUsecaseStub
	}
}

describe('SignupController Test', () => {
	describe('#handle', () => {
		test('should not save the user into the database because the password and passwordConfirmation isn\'t equals', async () => {
			const { sut, signupUsecaseStub } = makeSut()

			const signupUsecaseStubSpy = jest.spyOn(signupUsecaseStub, 'execute')

			const httpRequest = {
				body: {
					email: 'any_email@gmail.com',
					username: "username",
					password: 'any_password',
					passwordConfirmation: 'different_password'
				}
			}

			const response = await sut.handle(httpRequest)

			expect(signupUsecaseStubSpy).not.toBeCalled

			expect(response.statusCode).toBe(400)
			expect(response.body).toEqual(new InvalidParamError('passwordConfirmation'))
		})

		test('should not save the user into the database because the usecase throws', async () => {
			const { sut, signupUsecaseStub } = makeSut()

			const signupUsecaseStubSpy = jest.spyOn(signupUsecaseStub, 'execute')
			signupUsecaseStubSpy.mockRejectedValue(new Error('unexpected error'))

			const httpRequest = {
				body: {
					email: 'any_email@gmail.com',
					username: "username",
					password: 'any_password',
					passwordConfirmation: 'any_password'
				}
			}

			const response = await sut.handle(httpRequest)

			expect(signupUsecaseStubSpy).not.toBeCalled

			expect(response.statusCode).toBe(500)
			expect(response.body).toEqual(new ServerError())
		})

		test('should save the user into the database and return the token', async () => {
			const { sut } = makeSut()

			const httpRequest = {
				body: {
					email: 'any_email@gmail.com',
					username: "username",
					password: 'any_password',
					passwordConfirmation: 'any_password'
				}
			}

			const resposne = await sut.handle(httpRequest)

			expect(resposne.statusCode).toBe(200)
			expect(resposne.body).toStrictEqual("testid")
		})
	})
})