import { User, UserModel } from "../../domain/models/user.model";
import UseCase from "../../domain/models/usecase";
import { InvalidParamError } from "../errors";
import { Controller } from "../model/controller.interface";
import { HttpRequest, HttpResponse } from "../model/http.interface";
import { OK } from "../helpers/http.helpers";
import ControllerErrorHandler from "../helpers/ControllerErrorHandler";


export default class SignupController implements Controller<HttpRequest, HttpResponse> {
	constructor(
		private readonly signupUsecase: UseCase<UserModel, User>
	) {}
	
	@ControllerErrorHandler
    public async handle(request: HttpRequest): Promise<HttpResponse> {
        const {email, username, password, passwordConfirmation} = request.body
        if (password !== passwordConfirmation) {
        	throw new InvalidParamError('passwordConfirmation')
        }

        const signupUsecaseResult = await this.signupUsecase.execute({ 
        	email, username, password
        })

        return OK(signupUsecaseResult.id)
    }
}