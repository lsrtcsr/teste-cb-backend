import { Signin } from "../../domain/models/signin.model";
import { Token } from "../../domain/models/token.model";
import UseCase from "../../domain/models/usecase";
import { InvalidParamError } from "../errors";
import ControllerErrorHandler from "../helpers/ControllerErrorHandler";
import { OK } from "../helpers/http.helpers";
import { Controller } from "../model/controller.interface";
import { HttpRequest, HttpResponse } from "../model/http.interface";

export default class SigninController implements Controller<HttpRequest, HttpResponse> {
    constructor(private readonly signinUsecase: UseCase<Signin, Token>) {}

    @ControllerErrorHandler
    public async handle(request: HttpRequest): Promise<HttpResponse> {
        const {username, password} = request.body;
        if (!username || !password) {
            throw new InvalidParamError('username or password')
        }

        const token = await this.signinUsecase.execute({username, password})
        return OK(token)
    }	
}