import { Recipe, RecipeModel } from "../../domain/models/recipe.model";
import UseCase from "../../domain/models/usecase";
import GetRecipeByIdController from "./getRecipesById.controller";
import recipes from '../../../recipes.json'

interface SutType {
	sut: GetRecipeByIdController
	usecase: UseCase<string, Recipe>
}

const makeUsecase = (): UseCase<string, Recipe> => {
	class UsecaseStub implements UseCase<string, Recipe> {
        public async execute(data: string): Promise<Recipe> {
        	const mockedRecipe = recipes[0] as unknown as RecipeModel
            return {
            	id: 'testid',
            	...mockedRecipe
            }
        }
	}

	return new UsecaseStub()
}

const makeSut = (): SutType => {
	const usecase = makeUsecase()
	const sut = new GetRecipeByIdController(usecase)

	return {
		sut,
		usecase
	}
}