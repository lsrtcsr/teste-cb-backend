import { Signin } from '../../domain/models/signin.model'
import { Token } from '../../domain/models/token.model'
import UseCase from '../../domain/models/usecase'
import SigninController from './signin.controller'

interface SutType {
  sut: SigninController
  signinUsecaseStub: UseCase<Signin, Token>
}

const makeSigninUseCaseStub = (): UseCase<Signin, Token> => {
  class Usecase implements UseCase<Signin, Token> {
    public async execute (data: Signin): Promise<Token> {
      return {
            	token: 'testid',
            	refresh: 'testid'
      }
    }
  }

  return new Usecase()
}

const makeSut = (): SutType => {
  const signinUsecaseStub = makeSigninUseCaseStub()
  const sut = new SigninController(signinUsecaseStub)

  return {
    sut,
    signinUsecaseStub
  }
}
