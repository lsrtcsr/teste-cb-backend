import GetRecipeByIdUsecase from "../../application/recipes/getRecipeById.usecase";
import { Recipe } from "../../domain/models/recipe.model";
import UseCase from "../../domain/models/usecase";
import { MissingParamError, NotFoundError } from "../errors";
import { OK } from "../helpers/http.helpers";
import { Controller } from "../model/controller.interface";
import { HttpRequest, HttpResponse } from "../model/http.interface";

export default class GetRecipeByIdController implements Controller<HttpRequest, HttpResponse> {
	constructor(private readonly getRecipeById: UseCase<string, Recipe>) {}

    public async handle(request: HttpRequest): Promise<HttpResponse> {
        const { recipeId } = request.params
        if (!recipeId) {
        	throw new MissingParamError('recipeId')
        }

        const foundRecipe = await this.getRecipeById.execute(recipeId)
     	if (!foundRecipe) {
     		throw new NotFoundError(recipeId)
     	}

     	return OK(foundRecipe)
    }
}