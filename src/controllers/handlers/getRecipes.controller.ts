import { Pagination } from '../../domain/models/pagination.model'
import { Recipe } from '../../domain/models/recipe.model'
import UseCase from '../../domain/models/usecase'
import ControllerErrorHandler from '../helpers/ControllerErrorHandler'
import { OK } from '../helpers/http.helpers'
import { Controller } from '../model/controller.interface'
import { HttpRequest, HttpResponse } from '../model/http.interface'

export default class GetRecipesController implements Controller<HttpRequest, HttpResponse> {
  constructor (private readonly getRecipesUsecase: UseCase<Pagination, Recipe[]>) { }

  @ControllerErrorHandler
  public async handle (request: HttpRequest): Promise<HttpResponse> {
    const { page, perPage } = request.query
    const recipes = await this.getRecipesUsecase.execute({ page, perPage })

    return OK(recipes)
  }
}
