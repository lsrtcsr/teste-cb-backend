export default interface Decrypter {
	compare(encPassword: string, passwprd: string): Promise<boolean>
}