export default interface Tokenizer {
	tokenize(data: any): Promise<string>
}