export type Ingredient = {
	quantity: number,
	name: string
}