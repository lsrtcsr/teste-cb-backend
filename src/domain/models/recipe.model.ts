import { Ingredient } from "./ingredient.model";
import { PreparationStep } from "./peparationStep.model";

export type RecipeModel = {
	name: string
	description: string,
	peparationTime: string,
	ingredients: Ingredient[],
	preparationMode: PreparationStep[]
}

export type Recipe = RecipeModel & {
	id: string
}