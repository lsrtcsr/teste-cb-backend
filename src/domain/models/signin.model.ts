export type Signin = {
	username: string
	password: string
}