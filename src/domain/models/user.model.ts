export type UserModel = {
	username: string,
	password: string
	email: string
}

export type User = UserModel & {
	id: string
}