export type Signup = {
	email: string
	name: string
	password: string
	passwordConfirmation: string
}