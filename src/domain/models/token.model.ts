export type Token = {
	token: string,
	refresh: string
}