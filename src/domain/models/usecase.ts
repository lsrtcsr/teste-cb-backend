export default interface UseCase<T, O> {
	execute(data: T): Promise<O>
}